import java.util.Random;
public class RouletteWheel {
    
    //Private fields
    private Random randomNum; 
    private int spinNum; 

    //Constructor
    public RouletteWheel() {
        this.randomNum = new Random(); 
        this.spinNum = 0;
    }

    //Instance methods 
    public void spin() {
        this.spinNum = this.randomNum.nextInt(37); 
    }

    public int getValue() {
        return this.spinNum;
    }
}
