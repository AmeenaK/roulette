import java.util.Scanner;
public class Roulette {
    public static final Scanner scan = new Scanner(System.in);
    
    //Helper method to check if betNum is 0-36
    public static int checkBetNum(int betNum) {
        while (betNum < 0 || betNum >= 37) {
            System.out.println("Please enter a new valid number to bet on. You can only bet on numbers 0-36.");
            betNum = scan.nextInt();
        }
        System.out.println("The number you chose to bet on is " + betNum);
        return betNum;
    }

    //Helper method to check if the user can bet that amount of money.
    public static double checkBetAmount(double betAmount, double amountMoney) {
        while (betAmount > amountMoney || betAmount <= 0) {
            System.out.println("Please enter a new valid positive bet amount that is within your budget.");
            betAmount = scan.nextDouble();
        }
        System.out.println("The amount of money you chose to bet on is " + betAmount);
        return betAmount;
    }

    //Helper method to check if the user won or not.
    public static boolean checkIfWinning(int betNum, int spinNum) {
        boolean betGuess = false;
        if (betNum == spinNum) { 
            betGuess = true;
        }
        return betGuess;
    }

    //Helper method to calculate the amount of money after loss/win.
    public static double calculateMoney(double betAmount, double amountMoney, boolean betGuess) {
        if (betGuess) {
            amountMoney = amountMoney + (betAmount * 35);
            System.out.println("OH YES, you just WON: " + betAmount + " dollars. You now have " + amountMoney + " dollars." );
        }
        else  {
            amountMoney = amountMoney - betAmount;
            System.out.println("OH NO, you just LOST: " + betAmount + " dollars. You now have " + amountMoney + " dollars." );
        }
        return amountMoney;
    }


    //Main method
    public static void main(String[] args) {
        
        //Welcoming the user
        System.out.println("Hello, Welcome to Ameena's Roulette Game!");
        
        //Variables 
        RouletteWheel rouletteObject = new RouletteWheel();
        double amountMoney = 1000;
        boolean gameOver   = false;
        String answer; 
        int betNum;
        double betAmount;
        int spinNum;
        boolean betGuess;

        //Repeats until it is gameOver(User doesn't want to bet) or until the user doesn't have any money left to bet.
        while (!gameOver && amountMoney > 0) {

            //ASSUMPTION: User correctly enters "Yes." If any other answer, gameOver. 
            System.out.println("Would you like to make a bet?");
            answer = scan.next();

            if (answer.equals("Yes")) {
                
                //Validating betNum by calling helper method
                System.out.println("What number would you like to bet on?");
                betNum = scan.nextInt();
                betNum = checkBetNum(betNum);
        
                //Validating betAmount by calling helper method
                System.out.println("What amount of money do you want to bet?");
                betAmount = scan.nextDouble();
                betAmount = checkBetAmount(betAmount, amountMoney);
            
                //Spinning rouletteObject, assigning that to spinNum and printing it out.
                rouletteObject.spin();
                spinNum = rouletteObject.getValue(); 
                System.out.println("The number that landed on the roulette is " + spinNum);
               
                //Calling helper methods checkIfWinning & calculateMoney to see if player won or not and displaying amount of money left.
                betGuess    = checkIfWinning(betNum, spinNum);
                amountMoney = calculateMoney(betAmount, amountMoney, betGuess);
            }
            //Else: When user doesn't enter "Yes" to wanting to bet.
            else {
                gameOver = true;
            }
        }
        //Ending of the game.
        System.out.println("Ending the game. The user currently has " + amountMoney + " dollars");
    }
}